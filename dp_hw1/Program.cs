﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dp_hw1
{
    abstract class PrinterCreator
    {
        public abstract IPrint FactoryMethod();
        public string PrintOperation()
        {
            IPrint product = FactoryMethod();
            string result = product.Print();

            return result;
        }
    }

    class Canon : PrinterCreator
    {
        public override IPrint FactoryMethod()
        {
            return new Perl();
        }
    }

    class HP : PrinterCreator
    {
        public override IPrint FactoryMethod()
        {
            return new Offset();
        }
    }

    public interface IPrint
    {
        string Print();
    }

    class Perl : IPrint
    {
        public string Print()
        {
            return "Printed on Perl Paper";
        }
    }

    class Offset : IPrint
    {
        public string Print()
        {
            return "Printed on Offset Paper";
        }
    }

    class Client
    {
        public void Main()
        {
            Console.WriteLine("HP printed info");
            ClientCode(new HP());

            Console.WriteLine("");

            Console.WriteLine("Canon printed info");
            ClientCode(new Canon());
        }

        public void ClientCode(PrinterCreator creator)
        {
            Console.WriteLine("Paper Type: " + creator.PrintOperation());
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            new Client().Main();
            Console.ReadLine();
        }
    }
}
